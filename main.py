from slack import WebClient
from slack.errors import SlackApiError
import os
import certifi
import ssl


ssl_context = ssl.create_default_context(cafile=certifi.where())
token = os.environ.get('TOKEN')


def alerta(request):
    client = WebClient(token=token, ssl=ssl_context)
    try:
        mensagem = request.args.get("mensagem")
        r = client.chat_postMessage(channel='#alertas',
                                    text="{mensagem}".format(
                                                      mensagem=mensagem))
        return "Mensagem enviada com sucesso!"
    except SlackApiError as err:
        assert err.response["ok"] is False
        assert err.response["error"]
        return "Problemas com a conexão da API do slack, \
                favor verificar: {erro}".format(erro=err.response['error'])
